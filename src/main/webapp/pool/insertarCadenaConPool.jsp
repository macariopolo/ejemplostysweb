<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*, edu.uclm.esi.tysweb.pool.*, java.sql.*" %>

<%
	long timeIni=System.currentTimeMillis();
	String cadena=request.getParameter("cadena");
	int n=Integer.parseInt(request.getParameter("n"));
	
	for (int i=0; i<n; i++) {
		Connection bd=BrokerConPool.get().getBD();
		String sql="Insert into datos (texto) values (?)";
		PreparedStatement ps=bd.prepareStatement(sql);
		ps.setString(1, cadena);
		ps.executeUpdate();
		BrokerConPool.get().close(bd);
	}
	long timeFin=System.currentTimeMillis();
	
	JSONObject jso=new JSONObject().put("time", timeFin-timeIni);
	out.print(jso);
%>