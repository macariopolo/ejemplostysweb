<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Vector" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Manejo de sesión</title>

<%
session.setMaxInactiveInterval(10);
String dato=request.getParameter("p");
if (dato!=null && dato.equals("cerrar")) { 
	session.invalidate();
	session=request.getSession(true);
}
session.setAttribute("ultimoDato", dato);
Vector<String> vector;
if (session.getAttribute("vector")==null) {
	vector=new Vector<String>();
	session.setAttribute("vector", vector);
} else {
	vector=(Vector<String>) session.getAttribute("vector");
	vector.add(dato);
}
Integer visitas=(Integer) session.getAttribute("numeroDeVisitas");
if (visitas==null)
	visitas=1;
else visitas++;
session.setAttribute("numeroDeVisitas", visitas);
%>

</head>
<body>
	<h1>Esta sesión tiene el id: <%= session.getId() %></h1><br>

	<form method="get" action="manejoDeSesion.jsp">
		<input type="text" name="p" placeholder="Escribe aquí lo que desees" width="60"><br>
		<button type="submit">Enviar</button><br>
	</form>
	
	<%
	if (visitas==1)
		out.print("<h2>Es la primera vez que vienes en esta sesión de navegación, por lo que no tienes ningún objeto almacenado</h2>");
	else {
		%>
			<h2>Objetos almacenados en la sesión:</h2><br>
			<ul>
				<li>visitas: <%= visitas %></li>
				<li>ultimoDato: <%= session.getAttribute("ultimoDato") %></li>
				<li>vector: <%= session.getAttribute("vector") %></li>
			</ul>		
		<%
	}
	%>

</body>
</html>