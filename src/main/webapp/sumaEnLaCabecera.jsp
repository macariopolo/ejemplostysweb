<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<% response.addHeader("Access-Control-Allow-Origin", "*"); %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Calcula una suma y la devuelve en la cabecera</title>
</head>
<body>

	<%
	String sA=request.getParameter("a");
	String sB=request.getParameter("b");
	%>
	<form method="POST" action="sumaEnLaCabecera.jsp">
		a: <input type="text" name="a"><br>
		b: <input type="text" name="b"><br>
		<button onclick="submit">Sumar</button>
		<%
		int a, b, r;
		if (sA!=null && sB!=null) {
			a=Integer.parseInt(sA);
			b=Integer.parseInt(sB);
			r=a+b;
			response.addHeader("resultado", "" + a + " + " + b + " = "+ r);
		}
		%>
	</form>
	
	<%
		if (response.getHeader("resultado")!=null)
			out.println(response.getHeader("resultado"));
	%>
</body>
</html>