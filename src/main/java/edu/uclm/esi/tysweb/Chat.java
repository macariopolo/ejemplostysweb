package edu.uclm.esi.tysweb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/websockets/Chat")
public class Chat {
	private static List<Session> sesiones=Collections.synchronizedList(new ArrayList<>());
	
	@OnOpen
	public void onOpen(Session session) {
		sesiones.add(session);
		send(session, "Id de sesión: " + session.getId());
	}
	
	@OnMessage
	public void recibir(Session session, String msg) {
		synchronized (sesiones) {
		      Iterator<Session> iSesiones = sesiones.iterator(); // Must be in synchronized block
		      while (iSesiones.hasNext()) {
		    	  	Session s=iSesiones.next();
		    	  	if (s!=session)
		    	  		send(s, msg);
		      }
		  } 
	}
	
	public void send(Session session, String msg) {
		try {
			synchronized(session) {
				if (session.isOpen())
					session.getAsyncRemote().sendText(msg);
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
