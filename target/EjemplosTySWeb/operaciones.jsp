<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@page import="org.json.*" %>

<%
	double operando1=Double.parseDouble(request.getParameter("operando1"));
	char operador=request.getParameter("operador").charAt(0);
	double operando2=Double.parseDouble(request.getParameter("operando2"));
	
	Double resultado=null;
	switch (operador) {
	case '+' :
		resultado=operando1 + operando2; break;
	case '-' :
		resultado=operando1 - operando2; break;
	case '*' :
		resultado=operando1 * operando2; break;
	case '/' :
		resultado=operando1 / operando2; break;
	}
	JSONObject jso=new JSONObject();
	try {
		jso.put("resultado", resultado);
	} catch (JSONException e) { }
%>

<%= jso.toString() %>