<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Vector" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Manejo de sesión</title>

<%
String dato=request.getParameter("p");
session.setAttribute("ultimoDato", dato);
Vector<String> vector;
if (session.getAttribute("vector")==null) {
	vector=new Vector<>();
} else {
	vector=(Vector<String>) session.getAttribute("vector");
}
vector.add(dato);
%>

</head>
<body>
	<input type="text" id="dato" placeholder="Escribe aquí lo que desees"><br>
	<button onclick="enviar()">Enviar</button><br>
	
	<label id="sessionId">session.getId() = <%= session.getId() %></label><br>
	Objetos almacenados en la sesión:
	<label id="ultimoDato"><%= session.getAttribute("ultimoDato") %></label>
	<ul id="vector">
		<%
		for (int i=0; i<vector.size(); i++)
			out.print("<li>" + vector.get(i) + "</i>");
		%>
	</ul>
</body>

<script>
function enviar() {
	var request=new XMLHttpRequest();
	request.open("GET", "manejoDeSesion.jsp");
	request.onreadystatechange = function() {
		
	};
	var p = "p=" +  document.getElementById("dato").value;
	request.send(p);
}
</script>
</html>